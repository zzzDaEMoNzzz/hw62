import React, { Component } from 'react';
import './App.css';

import { Route, Switch, BrowserRouter } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Contacts from "./components/Contacts/Contacts";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Header />
          <div className="App-body">
            <Switch>
              <Route path="/about" component={About}/>
              <Route path="/contacts" component={Contacts}/>
              <Route path="/" exact component={Home}/>
            </Switch>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
