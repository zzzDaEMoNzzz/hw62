import React from 'react';
import './Header.css';
import { NavLink, Link } from "react-router-dom";

import Logo from './logo.png';

const Header = () => {
  return (
    <header className="Header">
      <Link to="/" className="logo"><img src={Logo} alt=""/></Link>
      <nav>
        <NavLink exact to="/">Home</NavLink>
        <NavLink to="/about">About us</NavLink>
        <NavLink to="/contacts">Contacts</NavLink>
      </nav>
    </header>
  );
};

export default Header;
