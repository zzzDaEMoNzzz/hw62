import React from 'react';
import './Footer.css';

const Footer = () => {
  return (
    <footer className="Footer">
      <span>Vladimir Kurlov © 2019</span>
    </footer>
  );
};

export default Footer;
