import React, {Fragment} from 'react';

const Home = () => {
  return (
    <Fragment>
      <h3>General Business Activities</h3>
      <p>Johnson Corporation has been established as a real estate company in North and South America. Our traditional business model is based on the accomplishment of properties in the real estate markets in America. Based on the decision of the company to diversify our properties; we have now established this corporation in Toronto. The revenues of our company are expected to be nearly US$50,000 per month depending on the variables that are factored in with investments in the real estate industry. There is a great need for certified or official bank checks in the future to deal with some real estate transactions. In addition to real estate investments, the company has invested portions of its assets in the purchase and sale of securities such as stocks and bonds as well as Forex trading on global markets.</p>
    </Fragment>
  );
};

export default Home;