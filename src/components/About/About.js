import React, {Fragment} from 'react';

const About = () => {
  return (
    <Fragment>
      <h3>Company Strategy</h3>
      <ul>
        <li><b>Purpose</b> To be a leader in the real estate industry by providing enhanced services, relationship and profitability.</li>
        <li><b>Vision</b> To provide quality services that exceeds the expectations of our esteemed customers.</li>
        <li><b>Mission statement</b> To build long term relationships with our customers and clients and provide exceptional customer services by pursuing business through innovation and advanced technology.</li>
        <li><b>Core values•</b> We believe in treating our customers with respect and faith• We grow through creativity, invention and innovation.• We integrate honesty, integrity and business ethics into all aspects of our business functioning</li>
        <li><b>Goals•</b> Regional expansion in the field of property management and develop a strong base of key customers.• Increase the assets and investments of the company to support the development of services.• To build good reputation in the field of real estate and property management and become a key player in the industry.</li>
      </ul>
    </Fragment>
  );
};

export default About;