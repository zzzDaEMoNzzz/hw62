import React, {Fragment} from 'react';

const Contacts = () => {
  return (
    <Fragment>
      <h3>Contacts</h3>
      <ul>
        <li><b>Business Name:</b> Johnson Corporation</li>
        <li><b>Business Address:</b> 153 James Street, Miami, USA</li>
        <li><b>Email:</b> Info@Johncorp.com</li>
        <li><b>Tel:</b> 555-777-9999</li>
      </ul>
    </Fragment>
  );
};

export default Contacts;